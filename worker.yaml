heat_template_version: 2015-04-30

description: Additional control plane nodes for your kubernetes


# note that this should depend on the router interface for the net
parameters:
  index:
    type: string
    label: node index
    description: This node is the N'th worker
  net_id:
    type: string
    immutable: true
    label: private network ID
    description: ID of the network to create the instance on
    constraints:
      - custom_constraint: neutron.network
  keypair:
    type: string
    immutable: true
    label: ssh key
    description: The name of the SSH key to add to gateway
    constraints:
      - custom_constraint: nova.keypair
  secgroup:
    type: string
    label: security group
    description: Security group for the kubernetes workers
    constraints:
      - custom_constraint: neutron.security_group
  flavor:
    type: string
    label: Worker flavor
    description: Resources to allocte for the worker instances
    constraints:
      - custom_constraint: nova.flavor
    default: mpcdf.large
  join_cmd:
    type: string
    label: kubernetes join command
    description: kubeadm command to join the cluster as worker
  user_data:
    type: string
    label: common Kubernetes onfiguration
    description: Configuration element to enable the machine to run kubernetes
  prefix:
    type: string
    label: Resource name prefix
    description: This string is prepended to all resource names
    default: "k8s"
  suffix:
    type: string
    label: Resource name suffix
    description: This string is appended to all resource names
    default: ""
  scheduler-hint:
    type: string
    label: instances placement policy
    description: Hint to OpenStack scheduler on how to place the instances
  containerd_volume_size:
    type: number
    label: volume size
    description: Size in GiB of volume use to house containerd root and state
    default: 20

resources:
  volume:
    type: OS::Cinder::Volume
    properties:
      size: {get_param: containerd_volume_size}
  user-data:
    type: OS::Heat::MultipartMime
    properties:
      parts:
        - config:
            str_replace:
              params:
                $VOLUME_ID: {get_resource: volume}
              template:
                get_file: containerd-volume.sh
        - config: {get_param: user_data}
        - config:
            str_replace:
              params:
                $JOIN_CMD: {get_param: join_cmd}
              template: |
                #!/bin/bash -x
                $JOIN_CMD
  instance:
    type: OS::Nova::Server
    properties:
      name:
        list_join:
          - "-"
          - - {get_param: prefix}
            - "worker"
            - {get_param: index}
            - {get_param: suffix}
      image: Debian 11
      flavor: {get_param: flavor}
      key_name: {get_param: keypair}
      networks:
        - network: {get_param: net_id}
      scheduler_hints:
        group: { get_param: scheduler-hint }
      security_groups:
        - {get_param: secgroup}
      user_data: {get_resource: user-data}
      user_data_update_policy: IGNORE
      user_data_format: "SOFTWARE_CONFIG"
  volume-attachment:
    type: OS::Cinder::VolumeAttachment
    properties:
      instance_uuid: {get_resource: instance}
      volume_id: {get_resource: volume}
