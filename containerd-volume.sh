#!/bin/bash
# Create a filesystem on attached volume and use it for containerd root and
# state
#   Variables supplied by HEAT:
#       $VOLUME_ID: OpenStack ID of volume

# --- container storage ---
mkdir /containerd

# --- build disk ID from OpenStack volume ID
vid=$(echo $VOLUME_ID | tr -d '-')
tgt="/dev/disk/by-id/virtio-${vid:0:8}-${vid:8:4}-${vid:12:4}-${vid:16:1}"

# --- create and mount the filesystem
mkfs.ext4 "$tgt"
echo "UUID=$(blkid -o value -s UUID /dev/vdb) /containerd ext4 rw,discard,errors=remount-ro,x-systemd.growfs 0 1" >> /etc/fstab
mount /containerd

# --- create directories for root and state data
mkdir /containerd/root /containerd/state
